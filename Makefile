export DOCKER_DEFAULT_PLATFORM=linux/amd64

up:
	sudo docker compose up --build

down:
	sudo docker compose down --remove-orphans