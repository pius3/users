import uvicorn
from fastapi import APIRouter, FastAPI
from fastapi.exceptions import RequestValidationError, HTTPException
from fastapi.responses import JSONResponse
from starlette import status

from src.core.settings import settings
from src.common.schemas import ErrorResponse, ErrorsResponse
from src.users.handlers import user_router, auth_router
from src.sellers.handlers import seller_router
from src.reviews.handlers import review_router

app = FastAPI(
    title=settings.PROJECT_NAME,
    debug=settings.DEBUG,
    version=settings.VERSION,
)


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    print(exc.__dict__)
    error = ErrorResponse(message="Validation error", code=status.HTTP_400_BAD_REQUEST)
    return JSONResponse(
        status_code=status.HTTP_400_BAD_REQUEST,
        content=ErrorsResponse(errors=error).dict()
    )


@app.exception_handler(HTTPException)
async def http_exception_handler(request, exc):
    error = ErrorResponse(message=exc.detail, code=exc.status_code)
    return JSONResponse(
        status_code=exc.status_code, content=ErrorsResponse(errors=error).dict()
    )

main_api_router = APIRouter(prefix="/api/v1", tags=["api"])

# set routes to the app instance
main_api_router.include_router(user_router, prefix="/users", tags=["users"])
main_api_router.include_router(seller_router, prefix="/sellers", tags=["sellers"])
main_api_router.include_router(review_router, prefix="/reviews", tags=["reviews"])
main_api_router.include_router(auth_router, prefix="/auth", tags=["auth"])
app.include_router(main_api_router)


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True, workers=3)
