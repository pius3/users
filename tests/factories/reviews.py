from factory import Faker, Sequence

from src.core.models import Review
from src.common.factory import AlchemyModelFactory


class ReviewFactory(AlchemyModelFactory):

    """Factory for creating Reviews."""

    class Meta:
        model = Review
        sqlalchemy_session_persistence = "commit"

    id = Sequence(lambda n: n)
    rating = Faker("random_int", min=1, max=5)
    description = Faker("sentence")
    created_at = Faker("date_time_this_year")
