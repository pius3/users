from factory import Faker, Sequence

from src.common.factory import AlchemyModelFactory
from src.core.models import User


class UserFactory(AlchemyModelFactory):

    """Factory for creating users."""

    class Meta:
        model = User
        sqlalchemy_session = ...

    id = Faker("uuid4")
    username = Sequence(lambda n: f"user{n}")
    password = "password"
    email = Sequence(lambda n: f"user{n}@example.com")
