from factory import Faker, Sequence, alchemy

from src.core.models import Seller
from src.common.factory import AlchemyModelFactory


class SellerFactory(AlchemyModelFactory):

    """Factory for creating Sellers."""

    class Meta:
        model = Seller
        sqlalchemy_session_persistence = "commit"

    id = Faker("uuid4")
    passport = Sequence(lambda n: 1234567890 + n)
