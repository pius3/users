import asyncio
from uuid import UUID

import pytest

from collections.abc import AsyncGenerator

from sqlalchemy import NullPool, delete
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from src.common.client import APIClient
from src.core.models.db import get_db
from src.core.settings import test_settings
from src.core.models.db import Base
from src.core.models import Review, Seller, User
from src.users.actions import _get_user_by_id, _create_new_user
from src.sellers.actions import _get_seller_by_id, _create_new_seller
from src.reviews.actions import _get_review_by_id, _create_new_review
from main import app

CLEAN_TABLES = [
    Review,
    Seller,
    User,
]

engine_test = create_async_engine(test_settings.test_db_url, poolclass=NullPool)
async_session_maker = sessionmaker(engine_test, class_=AsyncSession, expire_on_commit=False)
Base.metadata.bind = engine_test


async def override_get_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session


app.dependency_overrides[get_db] = override_get_session


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="function")
async def session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session


@pytest.fixture(autouse=True, scope="session")
async def prepare_database():
    async with engine_test.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield
    async with engine_test.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


@pytest.fixture(scope="session")
async def ac() -> AsyncGenerator[APIClient, None]:
    with APIClient(app=app) as ac:
        yield ac


@pytest.fixture(scope="function")
async def clean_db():
    async with async_session_maker() as session:
        async with session.begin():
            for table in CLEAN_TABLES:
                await session.execute(delete(table))


async def get_user_by_id(user_id: UUID) -> User | None:
    async with async_session_maker() as session:
        user = await _get_user_by_id(user_id, session)

    return user


async def create_user(username: str, email: str) -> User:
    user_data = {
        "username": username,
        "password": "test_password",
        "email": email,
    }
    async with async_session_maker() as session:
        user = await _create_new_user(user_data, session)

    return user


async def get_seller_by_id(seller_id: UUID) -> Seller | None:
    async with async_session_maker() as session:
        seller = await _get_seller_by_id(seller_id, session)

    return seller


async def create_seller(passport: int, user_id: UUID) -> Seller:
    seller_data = {
        "passport": passport,
        "user_id": user_id,
    }
    async with async_session_maker() as session:
        seller = await _create_new_seller(seller_data, session)

    return seller


async def get_review_by_id(review_id: int) -> Review | None:
    async with async_session_maker() as session:
        review = await _get_review_by_id(review_id, session)

    return review


async def create_review(rating: int, user_id: UUID, seller_id: UUID) -> Review:
    review_data = {
        "rating": rating,
        "description": "desc",
        "user_id": user_id,
        "seller_id": seller_id,
    }
    async with async_session_maker() as session:
        review = await _create_new_review(review_data, session)

    return review

