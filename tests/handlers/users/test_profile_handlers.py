import json
import uuid

from starlette import status

from tests.conftest import ac, get_user_by_id, create_user
from tests.handlers.users import BASE_URL

from src.common.client import APIClient
from src.core.models import User


async def test_get_user_profile(ac: APIClient, clean_db):
    user = await create_user(
        username="user_profile",
        email="user_profile@mail.ru",
    )
    response = ac.get(
        url=f"{BASE_URL}/profile/{user.id}",
    )
    response_data = json.loads(response.content)["data"]

    user_id = response_data["id"]

    assert response.status_code == status.HTTP_200_OK

    user = await get_user_by_id(user_id)
    assert type(user) is User
    assert response_data["username"] == user.username
    assert response_data["email"] == user.email


async def test_get_user_profile_not_found(ac: APIClient):
    user_id = uuid.uuid4()
    response = ac.get(
        url=f"{BASE_URL}/profile/{user_id}",
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND
