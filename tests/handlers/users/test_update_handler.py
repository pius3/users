import json

from starlette import status

from tests.conftest import ac, get_user_by_id, create_user
from tests.handlers.users import BASE_URL

from src.common.client import APIClient


async def test_update_user(ac: APIClient, clean_db):
    update_user_data = {
      "email": "user123@example.com"
    }
    user = await create_user(username="updated_user", email="updated_user@mail.ru")
    response = ac.patch(
        url=f"{BASE_URL}/{user.id}",
        data=update_user_data,
    )
    after_user = await get_user_by_id(user.id)
    response_data = json.loads(response.content)["data"]
    user_id = response_data["id"]
    assert response.status_code == status.HTTP_200_OK
    assert user_id == str(user.id)
    assert response_data["email"] == after_user.email
    assert response_data["username"] == after_user.username


async def test_update_user_wrong_email(ac: APIClient, clean_db):
    user = await create_user(username="test_update_wrong", email="test_update_wrong@mail.ru")
    user_data = {
      "email": "user.com"
    }
    response = ac.patch(
        url=f"{BASE_URL}/{user.id}",
        data=user_data,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
