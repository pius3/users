import json

from starlette import status

from common.client import APIClient
from tests.conftest import get_user_by_id
from tests.handlers.users import BASE_URL

from src.core.models import User


async def test_create_user(ac: APIClient, clean_db):
    user_data = {
      "username": "user",
      "password": "password",
      "email": "user@example.com"
    }
    response = ac.post(
        url=BASE_URL,
        data=user_data,
    )
    response_data = json.loads(response.content)["data"]
    user_id = response_data["id"]
    assert response.status_code == status.HTTP_200_OK
    assert response_data["username"] == user_data["username"]
    assert response_data["email"] == user_data["email"]

    user = await get_user_by_id(user_id)
    assert type(user) is User
    assert user.username == user_data["username"]


async def test_create_user_wrong_email(ac: APIClient, clean_db):
    user_data = {
      "username": "user",
      "password": "password",
      "email": "user.com"
    }
    response = ac.post(
        url=BASE_URL,
        data=user_data,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
