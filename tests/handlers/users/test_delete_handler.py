import json
import uuid

from starlette import status

from common.client import APIClient
from tests.conftest import ac, create_user, get_user_by_id
from tests.handlers.users import BASE_URL


async def test_delete_user(ac: APIClient):
    user = await create_user(username="delete_user", email="delete_user@mail.ru")

    response = ac.delete(
        url=f"{BASE_URL}/{user.id}",
    )
    response_data = json.loads(response.content)["data"]

    assert response.status_code == status.HTTP_200_OK
    assert response_data["id"] == str(user.id)
    assert await get_user_by_id(user.id) is None


async def test_delete_user_not_correct_id(ac: APIClient):
    response = ac.delete(
        url=f"{BASE_URL}/{uuid.uuid4()}",
    )
    response_data = json.loads(response.content)["data"]
    assert response.status_code == status.HTTP_200_OK
    assert response_data is None

