import json

from starlette import status

from common.client import APIClient
from tests.conftest import ac, get_review_by_id, create_review, create_seller, create_user
from tests.handlers.reviews import BASE_URL


async def test_delete_review(ac: APIClient, clean_db):
    user = await create_user(
        username="username_1",
        email="username_1@mail.ru",
    )
    user_for_seller = await create_user(
        username="username_2",
        email="username_2@mail.ru",
    )
    seller = await create_seller(
        passport=12345678,
        user_id=user_for_seller.id,
    )
    review = await create_review(
        rating=5,
        user_id=user.id,
        seller_id=seller.id,
    )
    response = ac.delete(
        url=f"{BASE_URL}/{review.id}",
    )
    response_data = json.loads(response.content)["data"]

    assert response.status_code == status.HTTP_200_OK
    assert response_data["id"] == review.id
    assert await get_review_by_id(review.id) is None


async def test_delete_review_not_correct_id(ac: APIClient):
    review_id = 155
    response = ac.delete(
        url=f"{BASE_URL}/{review_id}",
    )
    response_data = json.loads(response.content)["data"]

    assert response.status_code == status.HTTP_200_OK
    assert response_data is None
