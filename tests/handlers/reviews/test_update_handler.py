import json

from starlette import status

from common.client import APIClient
from tests.conftest import ac, get_review_by_id, create_review, create_user, create_seller
from tests.handlers.reviews import BASE_URL


async def test_update_review(ac: APIClient, clean_db):
    user = await create_user(
        username="username_1",
        email="username_1@mail.ru",
    )
    user_for_seller = await create_user(
        username="username_2",
        email="username_2@mail.ru",
    )
    seller = await create_seller(
        passport=12345678,
        user_id=user_for_seller.id,
    )
    review = await create_review(
        rating=5,
        user_id=user.id,
        seller_id=seller.id,
    )
    update_review_data = {
        "rating": 5,
        "description": "desc123",
    }
    response = ac.patch(
        url=f"{BASE_URL}/{review.id}",
        data=update_review_data,
    )
    response_data = json.loads(response.content)["data"]
    review_id = response_data["id"]
    before_review = await get_review_by_id(review.id)
    assert response.status_code == status.HTTP_200_OK
    assert review_id == review.id
    assert response_data["rating"] == before_review.rating
    assert response_data["description"] == before_review.description
    assert response_data["seller_id"] == str(before_review.seller_id)
    assert response_data["user_id"] == str(before_review.user_id)


async def test_update_review_wrong_seller_id(ac: APIClient):
    review_id = 155
    update_review_data = {
        "rating": 5,
        "description": "desc123",
    }
    response = ac.patch(
        url=f"{BASE_URL}/{review_id}",
        data=update_review_data,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND
