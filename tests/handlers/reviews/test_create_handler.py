import json
import uuid

from starlette import status

from tests.conftest import ac, get_review_by_id, create_user, create_seller
from tests.handlers.reviews import BASE_URL

from src.common.client import APIClient
from src.core.models import Review


async def test_create_review(ac: APIClient, clean_db, session):
    user = await create_user(
        username="username_1",
        email="username_1@mail.ru",
    )
    user_for_seller = await create_user(
        username="username_2",
        email="username_2@mail.ru",
    )
    seller = await create_seller(
        passport=12345678,
        user_id=user_for_seller.id,
    )
    review_data = {
        "rating": 5,
        "description": "desc",
        "user_id": str(user.id),
        "seller_id": str(seller.id),
    }
    response = ac.post(
        url=BASE_URL,
        data=review_data,
    )
    response_data = json.loads(response.content)["data"]
    review_id = response_data["id"]
    assert response.status_code == status.HTTP_200_OK
    assert response_data["rating"] == review_data["rating"]

    review = await get_review_by_id(review_id)
    assert type(review) is Review
    assert review.description == review_data["description"]


async def test_create_review_wrong_user(ac: APIClient, clean_db, session):
    user_for_seller = await create_user(
        username="username_2",
        email="username_2@mail.ru",
    )
    seller = await create_seller(
        passport=12345678,
        user_id=user_for_seller.id,
    )
    review_data = {
        "rating": 5,
        "description": "desc",
        "user_id": str(uuid.uuid4()),
        "seller_id": str(seller.id),
    }
    response = ac.post(
        url=BASE_URL,
        data=review_data,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
