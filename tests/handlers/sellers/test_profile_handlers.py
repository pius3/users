import json
import uuid

from starlette import status

from tests.conftest import ac, get_seller_by_id, create_seller, get_user_by_id, create_user, create_review
from tests.handlers.sellers import BASE_URL

from src.common.client import APIClient
from src.core.models import Seller


async def test_get_seller_profile(ac: APIClient, clean_db):
    user = await create_user(
        username="test_user",
        email="test_user@mail.ru",
    )
    seller = await create_seller(
        passport=12345678,
        user_id=user.id,
    )
    response = ac.get(
        url=f"{BASE_URL}/profile/{seller.id}"
    )
    response_data = json.loads(response.content)["data"]

    seller_id = response_data["id"]

    assert response.status_code == status.HTTP_200_OK

    seller = await get_seller_by_id(seller_id)
    assert type(seller) is Seller

    user = await get_user_by_id(seller.user_id)
    assert response_data["passport"] == seller.passport
    assert response_data["username"] == user.username
    assert response_data["email"] == user.email
    assert response_data["reviews"] == []
    assert response_data["avg_rating"] == 0


async def test_get_seller_profile_after_add_reviews(ac: APIClient, clean_db):
    user = await create_user(
        username="test_user",
        email="test_user@mail.ru",
    )
    seller = await create_seller(
        passport=12345678,
        user_id=user.id,
    )
    response = ac.get(
        url=f"{BASE_URL}/profile/{seller.id}"
    )
    response_data = json.loads(response.content)["data"]

    seller_id = response_data["id"]

    assert response.status_code == status.HTTP_200_OK

    seller = await get_seller_by_id(seller_id)
    assert type(seller) is Seller

    user = await get_user_by_id(seller.user_id)
    reviews = response_data["reviews"]
    assert response_data["passport"] == seller.passport
    assert response_data["username"] == user.username
    assert response_data["email"] == user.email
    assert len(reviews) == 0

    created_reviews = [(await create_review(rating=i, seller_id=seller.id, user_id=user.id)).rating for i in range(3)]

    response = ac.get(
        url=f"{BASE_URL}/profile/{seller.id}"
    )
    response_data = json.loads(response.content)["data"]

    assert response.status_code == status.HTTP_200_OK
    assert len(response_data["reviews"]) == len(created_reviews)
    assert response_data["avg_rating"] == sum(created_reviews) / len(created_reviews)