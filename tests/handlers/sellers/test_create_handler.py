import json
from datetime import timedelta

from starlette import status

from common.security import create_access_token
from core.settings import settings
from tests.conftest import ac, get_seller_by_id, create_user
from tests.handlers.sellers import BASE_URL

from src.common.client import APIClient
from src.core.models import Seller


async def test_create_seller_with_auth(ac: APIClient, clean_db):
    user = await create_user(
        username="username_1",
        email="username_1@mail.ru",
    )
    seller_data = {
        "passport": 12345678,
        "user_id": str(user.id),
    }
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.email},
        expires_delta=access_token_expires,
    )
    headers = {"Authorization": f"Bearer {access_token}"}
    response = ac.post(
        url=BASE_URL,
        data=seller_data,
        headers=headers,
    )
    assert response.status_code == status.HTTP_200_OK

    response_data = json.loads(response.content)["data"]
    seller_id = response_data["id"]
    seller = await get_seller_by_id(seller_id)

    assert type(seller) is Seller
    assert seller.passport == seller_data["passport"]
    assert str(seller.user_id) == seller_data["user_id"]
