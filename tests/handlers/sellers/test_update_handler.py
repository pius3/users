import json
import uuid

from starlette import status

from common.client import APIClient
from tests.conftest import ac, get_seller_by_id, create_seller, create_user
from tests.handlers.sellers import BASE_URL


async def test_update_seller(ac: APIClient, clean_db):
    user = await create_user(
        username="test_user",
        email="test_user@mail.ru",
    )
    seller = await create_seller(
        passport=12345678,
        user_id=user.id,
    )
    update_seller_data = {
        "passport": 456789,
    }
    response = ac.patch(
        url=f"{BASE_URL}/{seller.id}",
        data=update_seller_data,
    )
    response_data = json.loads(response.content)["data"]
    seller_id = response_data["id"]
    before_seller = await get_seller_by_id(seller.id)
    assert response.status_code == status.HTTP_200_OK
    assert seller_id == str(seller.id)
    assert response_data["passport"] == before_seller.passport


async def test_update_seller_wrong_review_id(ac: APIClient):
    update_seller_data = {
        "passport": 123456,
    }
    seller_id = uuid.uuid4()
    response = ac.patch(
        url=f"{BASE_URL}/{seller_id}",
        data=update_seller_data,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND
