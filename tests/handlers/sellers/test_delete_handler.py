import json
import uuid

from starlette import status

from common.client import APIClient
from tests.conftest import ac, get_seller_by_id, create_seller, create_user
from tests.handlers.sellers import BASE_URL


async def test_delete_seller(ac: APIClient, clean_db):
    user = await create_user(
        username="sel_username",
        email="sel_username@mail.ru",
    )
    seller = await create_seller(
        passport=34513456,
        user_id=user.id,
    )
    response = ac.delete(
        url=f"{BASE_URL}/{seller.id}",
    )
    response_data = json.loads(response.content)["data"]

    assert response.status_code == status.HTTP_200_OK
    assert response_data["id"] == str(seller.id)
    assert await get_seller_by_id(seller.id) is None


async def test_delete_seller_not_correct_id(ac: APIClient, clean_db):
    seller_id = uuid.uuid4()
    response = ac.delete(
        url=f"{BASE_URL}/{seller_id}",
    )
    response_data = json.loads(response.content)["data"]

    assert response.status_code == status.HTTP_200_OK
    assert response_data is None
