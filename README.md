# Service: users 


## Endpoints
it's first design API endpoints, that can be changed later on

| Method | Endpoint                   | Description                                        |
|--------|----------------------------|----------------------------------------------------|
| POST   | /api/users/                | Register User                                      |
| GET    | /api/users/profile/{id}    | Show User information                              |
| POST   | /api/users/auth/           | Auth User                                          |
| PATCH  | /api/users/{id}/           | Update User                                        |
| DELETE | /api/users/{id}/           | Delete User                                        |
| POST   | /api/sellers/              | Create Seller                                      |
| PATCH  | /api/sellers/{id}/         | Update Seller                                      |
| DELETE | /api/sellers/{id}/         | Delete Seller                                      |
| GET    | /api/sellers/profile/{id}/ | Get statistics about seller (use another services) |
| POST   | /api/reviews/              | Create Reviews about Seller                        | 
| GET    | /api/reviews/{id}/         | Get review for id                                  |   
| PATCH  | /api/reviews/{id}/         | Update Review                                      |
| DELETE | /api/reviews/{id}/         | Delete Review                                      |


## DB schema

<img src="db_schema.png">

