import uuid

from sqlalchemy import Boolean, Column, ForeignKey, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from .db import Base


class Seller(Base):
    __tablename__ = "sellers"

    id = Column(UUID, primary_key=True, unique=True, index=True, default=uuid.uuid4)
    passport = Column(Integer, unique=True, index=True)
    is_active = Column(Boolean, default=True)
    verified = Column(Boolean, default=False)
    user_id = Column(UUID, ForeignKey("users.id"), index=True, unique=True)
    user = relationship("User", back_populates="seller")
    reviews = relationship("Review", back_populates="seller")
