import uuid
from datetime import datetime

from sqlalchemy import Boolean, Column, DateTime, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import validates, relationship

from src.common.validators import validate_email

from .db import Base


class User(Base):

    """Model for a user."""

    __tablename__ = "users"

    id = Column(UUID, primary_key=True, index=True, default=uuid.uuid4)
    username = Column(String(length=32), unique=True)
    password = Column(String)
    email = Column(String(64), unique=True)
    firstname = Column(String(length=32), nullable=True)
    lastname = Column(String(length=32), nullable=True)
    status = Column(String(length=128), nullable=True)
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=datetime.now)
    seller = relationship("Seller", back_populates="user")
    reviews = relationship("Review", back_populates="user")

    @validates("email")
    def validate_email(self, key: str, email: str) -> str:
        return validate_email(email)
