from datetime import datetime

from sqlalchemy import Column, DateTime, ForeignKey, Integer, Text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship, validates

from .db import Base


class Review(Base):

    """Model for a review."""

    __tablename__ = "reviews"

    id = Column(Integer, primary_key=True, index=True)
    rating = Column(Integer)
    description = Column(Text, nullable=True)
    created_at = Column(DateTime, default=datetime.now)
    seller_id = Column(UUID, ForeignKey("sellers.id"), index=True)
    user_id = Column(UUID, ForeignKey("users.id"), index=True)
    seller = relationship("Seller", back_populates="reviews")
    user = relationship("User", back_populates="reviews")

    @validates("rating")
    def validate_rating(self, key: str, value: int) -> int:
        min_rating, max_rating = 0, 5
        if min_rating <= value <= max_rating:
            return value
        raise ValueError("Rating must be between 0 and 5")
