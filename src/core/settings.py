import os

from dotenv import load_dotenv

load_dotenv()


class __DBSettings:

    """DB connection settings projects."""

    DB_USERNAME: str = os.environ.get("POSTGRES_USER")
    DB_PASSWORD: str = os.environ.get("POSTGRES_PASSWORD")
    DB_NAME: str = os.environ.get("POSTGRES_NAME")
    DB_PORT: str = os.environ.get("POSTGRES_PORT")
    DB_HOST: str = os.environ.get("DB_HOST")

    @property
    def db_url(self):
        return f"postgresql+asyncpg://{self.DB_USERNAME}:{self.DB_PASSWORD}@{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}"


class Settings(__DBSettings):

    """Setting for projects."""

    PROJECT_NAME: str = os.environ.get("PROJECT_NAME")
    API_PREFIX: str = os.environ.get("API_PREFIX")
    DEBUG: bool = os.environ.get("DEBUG")
    VERSION: str = os.environ.get("VERSION")
    SECRET_KEY: str = os.environ.get("SECRET_KEY", default="secret_key")
    ALGORITHMS: str = os.environ.get("ALGORITHMS", default="HS256")
    ACCESS_TOKEN_EXPIRE_MINUTES: int = os.environ.get("ACCESS_TOKEN_EXPIRE_MINUTES", default=30)


class TestDBSettings:

    """TEST DB connection settings projects."""

    TEST_DB_USERNAME: str = os.environ.get("TEST_POSTGRES_USER")
    TEST_DB_PASSWORD: str = os.environ.get("TEST_POSTGRES_PASSWORD")
    TEST_DB_NAME: str = os.environ.get("TEST_POSTGRES_NAME")
    TEST_DB_PORT: str = os.environ.get("TEST_POSTGRES_PORT")
    TEST_DB_HOST: str = os.environ.get("TEST_DB_HOST")

    @property
    def test_db_url(self):
        return f"postgresql+asyncpg://{self.TEST_DB_USERNAME}:{self.TEST_DB_PASSWORD}@{self.TEST_DB_HOST}:{self.TEST_DB_PORT}/{self.TEST_DB_NAME}"


settings = Settings()
test_settings = TestDBSettings()
