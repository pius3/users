from uuid import UUID

from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from src.core.models import User
from src.reviews.schemas import ReviewInformation
from src.core.models.db import get_db
from src.reviews.actions import (
    _get_average_rating_by_seller_id,
    _get_reviews_by_seller_id,
)
from src.sellers.actions import (
    _create_new_seller,
    _delete_seller_by_id,
    _get_seller_by_id,
    _update_seller_by_id,
)
from src.common.schemas import ErrorsResponse
from src.sellers.schemas import (
    CreateSellerRequest,
    DefaultSellerResponse,
    DeleteSellerResponse,
    ProfileSellerResponse,
    DataProfileSellerResponse,
    DataDefaultSellerResponse,
    UpdateSellerRequest,
    DataDeleteSellerResponse,
)
from src.users.actions import _get_user_by_id
from src.users.utils import get_current_user_from_token

seller_router = APIRouter()


@seller_router.post(
    "/",
    response_model=DataDefaultSellerResponse,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_401_UNAUTHORIZED: {"description": "Unauthorized", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORT!"},
    }
)
async def create_seller(body: CreateSellerRequest, current_user: User = Depends(get_current_user_from_token), db: AsyncSession = Depends(get_db)) -> DataDefaultSellerResponse:
    body = {
        "passport": body.passport,
        "user_id": current_user.id,
    }

    seller = await _create_new_seller(body, db)
    data = DefaultSellerResponse(
        id=seller.id,
        passport=seller.passport,
        is_active=seller.is_active,
        verified=seller.verified,
        user_id=seller.user_id,
    )
    return DataDefaultSellerResponse(
        data=data
    )


@seller_router.get(
    "/profile/{seller_id}",
    response_model=DataProfileSellerResponse,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_404_NOT_FOUND: {"description": "Not Found", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORT!"},
    }
)
async def show_profile_seller(seller_id: UUID, db: AsyncSession = Depends(get_db)) -> DataProfileSellerResponse:
    seller = await _get_seller_by_id(seller_id, db)
    user = await _get_user_by_id(seller.user_id, db)
    reviews = [ReviewInformation(
        id=review.id,
        rating=review.rating,
        description=review.description,
        created_at=review.created_at,
        user_id=review.user_id,
        seller_id=review.seller_id,
    )
        for review in await _get_reviews_by_seller_id(seller_id, db)
    ]
    avg_rating = await _get_average_rating_by_seller_id(seller_id, db)

    if seller is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"Seller with id {seller_id} not found."
        )

    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User which related with seller not found"
        )

    data = ProfileSellerResponse(
        id=seller.id,
        username=user.username,
        email=user.email,
        firstname=user.firstname,
        lastname=user.lastname,
        status=user.status,
        created_at=user.created_at,
        passport=seller.passport,
        is_active=seller.is_active,
        verified=seller.verified,
        reviews=reviews,
        avg_rating=avg_rating,
    )
    return DataProfileSellerResponse(
        data=data
    )


@seller_router.patch(
    "/{seller_id}",
    response_model=DataDefaultSellerResponse,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_404_NOT_FOUND: {"description": "Not Found", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORT!"},
    }
)
async def update_seller_by_id(seller_id: UUID, body: UpdateSellerRequest, db: AsyncSession = Depends(get_db)) -> DataDefaultSellerResponse:
    updated_seller_data = body.dict(exclude_none=True)
    if not updated_seller_data:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="At least one parameter for seller update info should be provided",
        )
    updated_seller = await _get_seller_by_id(seller_id, db)
    if updated_seller is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"seller with id {seller_id} not found."
        )
    try:
        updated_seller = await _update_seller_by_id(
            seller_id=seller_id, updated_seller_data=updated_seller_data, session=db
        )
    except IntegrityError as err:
        raise HTTPException(status_code=500, detail=f"Database error: {err}")
    data = DefaultSellerResponse(
        id=updated_seller.id,
        passport=updated_seller.passport,
        is_active=updated_seller.is_active,
        verified=updated_seller.verified,
        user_id=updated_seller.user_id,
    )
    return DataDefaultSellerResponse(
        data=data
    )


@seller_router.delete(
    "/{seller_id}",
    response_model=DataDeleteSellerResponse,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORT!"},
    }
)
async def delete_seller(seller_id: UUID, db: AsyncSession = Depends(get_db)) -> DataDeleteSellerResponse:
    del_seller_id = await _delete_seller_by_id(seller_id, db)
    data = None
    if del_seller_id is not None:
        data = DeleteSellerResponse(id=del_seller_id)
    return DataDeleteSellerResponse(
        data=data
    )
