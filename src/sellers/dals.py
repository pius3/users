from uuid import UUID

from sqlalchemy import and_, select, update
from sqlalchemy.ext.asyncio import AsyncSession

from src.core.models import Seller


class SellerDAL:

    """Data Access Layer for operating seller info
    (Слой для работы с ORM | БД)
    """

    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session

    async def create_seller(
        self,
        passport: int,
        user_id: UUID,
    ) -> Seller:

        new_seller = Seller(
            passport=passport,
            user_id=user_id,
        )
        self.db_session.add(new_seller)
        await self.db_session.flush()
        return new_seller

    async def get_seller_by_id(self, seller_id: UUID) -> Seller | None:
        query = select(Seller).where(and_(Seller.id == seller_id, Seller.is_active == True))
        res = await self.db_session.execute(query)
        seller_row = res.fetchone()
        if seller_row is not None:
            return seller_row[0]

    async def update_seller(self, seller_id: UUID, **kwargs: dict) -> Seller | None:
        query = (
            update(Seller)
            .where(and_(Seller.id == seller_id, Seller.is_active == True))
            .values(kwargs)
            .returning(Seller)
        )
        res = await self.db_session.execute(query)
        update_seller_id_row = res.fetchone()
        if update_seller_id_row is not None:
            return update_seller_id_row[0]

    async def delete_seller(self, seller_id: UUID) -> UUID | None:
        query = (
            update(Seller)
            .where(and_(Seller.id == seller_id, Seller.is_active == True))
            .values(is_active=False)
            .returning(Seller.id)
        )
        res = await self.db_session.execute(query)
        deleted_seller_id_row = res.fetchone()
        if deleted_seller_id_row is not None:
            return deleted_seller_id_row[0]

