from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from src.core.models import Seller
from src.sellers.dals import SellerDAL

"""
Файл, работает с сессией в БД
"""


async def _create_new_seller(body: dict, session: AsyncSession) -> Seller:
    async with session.begin():
        seller_dal = SellerDAL(session)
        seller = await seller_dal.create_seller(
            **body
        )
        return seller


async def _get_seller_by_id(seller_id: UUID, session: AsyncSession) -> Seller | None:
    async with session.begin():
        seller_dal = SellerDAL(session)
        seller = await seller_dal.get_seller_by_id(seller_id)
        if seller is not None:
            return seller


async def _update_seller_by_id(seller_id: UUID, updated_seller_data: dict, session: AsyncSession) -> Seller | None:
    async with session.begin():
        seller_dal = SellerDAL(session)
        upd_seller = await seller_dal.update_seller(seller_id, **updated_seller_data)
        return upd_seller


async def _delete_seller_by_id(seller_id: UUID, session: AsyncSession) -> UUID | None:
    async with session.begin():
        seller_dal = SellerDAL(session)
        del_seller = await seller_dal.delete_seller(seller_id)
        return del_seller
