from datetime import datetime
from uuid import UUID

from pydantic import BaseModel, EmailStr

from src.reviews.schemas import ReviewInformation
from src.common.schemas import TunedModel


class CreateSellerRequest(BaseModel):

    """Create seller with Pydantic."""

    passport: int


class UpdateSellerRequest(BaseModel):

    """Update seller with Pydantic."""

    passport: int | None = None
    is_active: bool | None = None
    verified: bool | None = None


class SellerIDResponse(TunedModel):

    """Parent class for ID seller response with Pydantic."""

    id: UUID


class DefaultSellerResponse(SellerIDResponse):

    """Response after create seller with Pydantic."""

    passport: int
    is_active: bool
    verified: bool
    user_id: UUID


class ProfileSellerResponse(SellerIDResponse):

    """Profile Seller with Pydantic."""

    username: str
    email: EmailStr
    firstname: str | None
    lastname: str | None
    status: str | None
    created_at: datetime
    passport: int
    is_active: bool
    verified: bool
    reviews: list[ReviewInformation]
    avg_rating: float


class DataDefaultSellerResponse(TunedModel):

    """"Wrapper for response, data object and response."""

    data: DefaultSellerResponse


class DataProfileSellerResponse(TunedModel):

    """Wrapper for response, data object and response."""

    data: ProfileSellerResponse


class DeleteSellerResponse(TunedModel):

    """Response after delete seller with Pydantic."""

    id: UUID


class DataDeleteSellerResponse(TunedModel):

    """Wrapper response after delete seller with Pydantic."""

    data: DeleteSellerResponse | None
