from collections.abc import Sequence
from uuid import UUID

from sqlalchemy import and_, delete, func, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from src.core.models import Seller
from src.core.models import Review


class ReviewDAL:

    """Data Access Layer for operating review info
    (Слой для работы с ORM | БД).
    """

    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session

    async def create_review(
        self,
        rating: int,
        description: str,
        seller_id: UUID,
        user_id: UUID,
    ) -> Review:

        new_review = Review(
            rating=rating,
            description=description,
            seller_id=seller_id,
            user_id=user_id,
        )
        self.db_session.add(new_review)
        await self.db_session.flush()
        return new_review

    async def get_review_by_id(self, review_id: int) -> Review | None:
        query = select(Review).where(Review.id == review_id)
        res = await self.db_session.execute(query)
        review_row = res.fetchone()
        if review_row is not None:
            return review_row[0]

    async def get_reviews_by_seller_id(self, seller_id: UUID) -> Sequence[Review] | list:
        query = (
            select(Review)
            .join(Seller, Review.seller_id == Seller.id)
            .where(and_(Review.seller_id == seller_id, Seller.is_active == True))
            .options(joinedload(Review.seller))
        )
        res = await self.db_session.scalars(query)
        reviews = res.all()
        if len(reviews) > 0:
            return reviews
        return list()

    async def get_average_rating_by_seller_id(self, seller_id: UUID) -> float:
        query = (
            select(func.avg(Review.rating))
            .filter(
                and_(Review.seller_id == seller_id, Review.seller.has(is_active=True))
            )
        )
        return await self.db_session.scalar(query)

    async def update_review(self, review_id: int, **kwargs) -> Review | None:
        query = (
            update(Review)
            .where(Review.id == review_id)
            .values(kwargs)
            .returning(Review)
        )
        res = await self.db_session.execute(query)
        update_review_id_row = res.fetchone()
        if update_review_id_row is not None:
            return update_review_id_row[0]

    async def delete_review(self, review_id: int) -> int | None:
        query = (
            delete(Review)
            .where(Review.id == review_id)
            .returning(Review.id)
        )
        res = await self.db_session.execute(query)
        deleted_review_id_row = res.fetchone()
        if deleted_review_id_row is not None:
            return deleted_review_id_row[0]

