from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from src.core.models import Review
from src.reviews.dals import ReviewDAL

"""
Файл, работает с сессией в БД
"""


async def _create_new_review(body: dict, session: AsyncSession) -> Review:
    async with session.begin():
        review_dal = ReviewDAL(session)
        review = await review_dal.create_review(
            **body
        )
        return review


async def _get_review_by_id(review_id: int, session: AsyncSession) -> Review | None:
    async with session.begin():
        review_dal = ReviewDAL(session)
        review = await review_dal.get_review_by_id(review_id)
        return review


async def _get_reviews_by_seller_id(seller_id: UUID, session: AsyncSession) -> list[Review]:
    async with session.begin():
        review_dal = ReviewDAL(session)
        reviews = await review_dal.get_reviews_by_seller_id(seller_id=seller_id)
        return reviews


async def _get_average_rating_by_seller_id(seller_id: UUID, session: AsyncSession) -> float:
    async with session.begin():
        review_dal = ReviewDAL(session)
        avg_rating = await review_dal.get_average_rating_by_seller_id(seller_id=seller_id)
        return avg_rating if avg_rating is not None else 0.0


async def _update_review_by_id(review_id: int, updated_review_data: dict, session: AsyncSession) -> Review | None:
    async with session.begin():
        review_dal = ReviewDAL(session)
        upd_review = await review_dal.update_review(review_id, **updated_review_data)
        return upd_review


async def _delete_review_by_id(review_id: int, session: AsyncSession) -> UUID | None:
    async with session.begin():
        review_dal = ReviewDAL(session)
        del_review = await review_dal.delete_review(review_id)
        return del_review
