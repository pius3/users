from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from src.core.models.db import get_db
from src.users.actions import _get_user_by_id
from src.sellers.actions import _get_seller_by_id
from src.reviews.actions import (
    _create_new_review,
    _delete_review_by_id,
    _get_review_by_id,
    _update_review_by_id,
)
from src.common.schemas import ErrorsResponse
from src.reviews.schemas import (
    CreateReviewRequest,
    DeleteReviewResponse,
    UpdateReviewRequest,
    ReviewInformation,
    DataReviewInformation,
    DataDeleteReviewResponse,
)

review_router = APIRouter()


@review_router.post(
    "/",
    response_model=DataReviewInformation,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORT!"},
    }
)
async def create_review(body: CreateReviewRequest, db: AsyncSession = Depends(get_db)) -> DataReviewInformation:
    user = await _get_user_by_id(user_id=body.user_id, session=db)
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User not found",
        )

    seller = await _get_seller_by_id(seller_id=body.seller_id, session=db)
    if seller is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Seller not found",
        )

    body = {
        "rating": body.rating,
        "description": body.description,
        "seller_id": body.seller_id,
        "user_id": body.user_id,
    }
    review = await _create_new_review(body, db)
    data = ReviewInformation(
        id=review.id,
        rating=review.rating,
        created_at=review.created_at,
        description=review.description,
        seller_id=review.seller_id,
        user_id=review.user_id,
    )
    return DataReviewInformation(
        data=data
    )


@review_router.patch(
    "/{review_id}",
    response_model=DataReviewInformation,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_404_NOT_FOUND: {"description": "Not found", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORT!"},
    }
)
async def update_review_by_id(review_id: int, body: UpdateReviewRequest, db: AsyncSession = Depends(get_db)) -> DataReviewInformation:
    updated_review_data = body.dict(exclude_unset=True)
    if not updated_review_data:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="At least one parameter for review update info should be provided",
        )
    updated_review = await _get_review_by_id(review_id, db)
    if updated_review is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"Review with id {review_id} not found."
        )
    try:
        updated_review = await _update_review_by_id(
            review_id=review_id, updated_review_data=updated_review_data, session=db
        )
    except IntegrityError as err:
        raise HTTPException(status_code=500, detail=f"Database error: {err}")
    data = ReviewInformation(
        id=updated_review.id,
        rating=updated_review.rating,
        created_at=updated_review.created_at,
        description=updated_review.description,
        seller_id=updated_review.seller_id,
        user_id=updated_review.user_id,
    )
    return DataReviewInformation(
        data=data
    )


@review_router.delete(
    "/{review_id}",
    response_model=DataDeleteReviewResponse,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORT!"},
    }
)
async def delete_review(review_id: int, db: AsyncSession = Depends(get_db)) -> DataDeleteReviewResponse:
    del_review_id = await _delete_review_by_id(review_id, db)
    data = None
    if del_review_id is not None:
        data = DeleteReviewResponse(id=del_review_id)
    return DataDeleteReviewResponse(
        data=data
    )
