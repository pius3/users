from datetime import datetime
from uuid import UUID

from fastapi import HTTPException, Depends
from pydantic import BaseModel, field_validator
from sqlalchemy.ext.asyncio import AsyncSession

from src.core.models.db import get_db
from src.common.schemas import TunedModel
from src.sellers.actions import _get_seller_by_id
from src.users.actions import _get_user_by_id


class CreateReviewRequest(BaseModel):

    """Create Review with Pydantic."""

    rating: int
    description: str | None = None
    seller_id: UUID
    user_id: UUID

    @field_validator("user_id")
    def validate_user(cls, value, db: AsyncSession = Depends(get_db)) -> UUID:
        """Проверка на привязку активного пользователя"""
        user = _get_user_by_id(value, db)
        if user is None:
            raise HTTPException(
                status_code=400, detail="User does not exist or don't active"
            )
        return value

    @field_validator("seller_id")
    def validate_seller(cls, value, db: AsyncSession = Depends(get_db)) -> UUID:
        """Проверка на привязку активного продавца"""
        seller = _get_seller_by_id(value, db)
        if seller is None:
            raise HTTPException(
                status_code=400, detail="Seller does not exist or don't active"
            )
        return value


class UpdateReviewRequest(BaseModel):

    """Update Review with Pydantic."""

    rating: int | None = None
    description: str | None = None


class ReviewIDResponse(TunedModel):

    """Parent class for ID Review response with Pydantic."""

    id: int


class ReviewInformation(ReviewIDResponse):

    """Response after retrieve/create/update."""

    rating: int
    description: str | None
    created_at: datetime
    seller_id: UUID
    user_id: UUID


class DataReviewInformation(TunedModel):

    """Wrapper for response, data object and response."""

    data: ReviewInformation


class DeleteReviewResponse(TunedModel):

    """Response after delete Review with Pydantic."""

    id: int


class DataDeleteReviewResponse(TunedModel):

    """Wrapper response after delete Review with Pydantic."""

    data: DeleteReviewResponse | None

