from datetime import timedelta

from fastapi import APIRouter, HTTPException, Depends
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from src.common.security import create_access_token
from src.core.settings import settings
from src.core.models.db import get_db
from src.common.schemas import ErrorsResponse
from src.users.actions import _auth_user
from src.users.schemas import Token

auth_router = APIRouter()


@auth_router.post(
    "/token",
    response_model=Token,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_401_UNAUTHORIZED: {"description": "Unauthorized", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORTED!"},
    }
)
async def token(form_data: OAuth2PasswordRequestForm = Depends(), db: AsyncSession = Depends(get_db)) -> Token:
    user = await _auth_user(form_data.username, form_data.password, db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
        )
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.email},
        expires_delta=access_token_expires,
    )
    return Token(
        access_token=access_token, token_type="bearer"
    )
