from uuid import UUID

from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from src.core.models.db import get_db
from src.users.actions import (
    _create_new_user,
    _delete_user_by_id,
    _get_user_by_id,
    _update_user_by_id,
)
from src.common.schemas import ErrorsResponse
from src.users.schemas import (
    CreateUserRequest,
    UpdateUserRequest,
    ProfileUserResponse,
    DeleteUserResponse,
    DataProfileUserResponse,
    DataDeleteUserResponse,
)
from src.common.hashing import Hasher

user_router = APIRouter()


@user_router.post(
    "/",
    response_model=DataProfileUserResponse,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORT!"},
    }
)
async def create_user(body: CreateUserRequest, db: AsyncSession = Depends(get_db)) -> DataProfileUserResponse:
    body = {
        "username": body.username.lower(),
        "password": Hasher.get_password_hash(body.password),
        "email": body.email,
        "firstname": body.firstname,
        "lastname": body.lastname,
        "status": body.status,
    }
    user = await _create_new_user(body, db)
    data = ProfileUserResponse(
            id=user.id,
            username=user.username,
            email=user.email,
            firstname=user.firstname,
            lastname=user.lastname,
            status=user.status,
            is_active=user.is_active,
            created_at=user.created_at,
        )
    return DataProfileUserResponse(
        data=data
    )


@user_router.get(
    "/profile/{user_id}",
    response_model=DataProfileUserResponse,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_404_NOT_FOUND: {"description": "Not Found", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORT!"},
    }
)
async def show_profile_user(user_id: UUID, db: AsyncSession = Depends(get_db)) -> DataProfileUserResponse:
    user = await _get_user_by_id(user_id, db)
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"User with id {user_id} not found."
        )
    data = ProfileUserResponse(
        id=user.id,
        username=user.username,
        email=user.email,
        firstname=user.firstname,
        lastname=user.lastname,
        status=user.status,
        is_active=user.is_active,
        created_at=user.created_at,
    )
    return DataProfileUserResponse(
        data=data
    )


@user_router.patch(
    "/{user_id}",
    response_model=DataProfileUserResponse,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_404_NOT_FOUND: {"description": "Not Found", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORT!"},
    }
)
async def update_user_by_id(user_id: UUID, body: UpdateUserRequest, db: AsyncSession = Depends(get_db)) -> DataProfileUserResponse:
    updated_user_data = body.dict(exclude_unset=True)
    if not updated_user_data:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="At least one parameter for user update info should be provided",
        )
    updated_user = await _get_user_by_id(user_id, db)
    if updated_user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"User with id {user_id} not found."
        )
    try:
        updated_user = await _update_user_by_id(
            user_id=user_id, updated_user_data=updated_user_data, session=db
        )
    except IntegrityError as err:
        raise HTTPException(status_code=500, detail=f"Database error: {err}")
    data = ProfileUserResponse(
        id=updated_user.id,
        username=updated_user.username,
        email=updated_user.email,
        firstname=updated_user.firstname,
        lastname=updated_user.lastname,
        status=updated_user.status,
        is_active=updated_user.is_active,
        created_at=updated_user.created_at,
    )
    return DataProfileUserResponse(
        data=data
    )


@user_router.delete(
    "/{user_id}",
    response_model=DataDeleteUserResponse,
    responses={
        status.HTTP_400_BAD_REQUEST: {"description": "Bad Request", "model": ErrorsResponse},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "NOT SUPPORT!"},
    }
)
async def delete_user(user_id: UUID, db: AsyncSession = Depends(get_db)) -> DataDeleteUserResponse:
    del_user_id = await _delete_user_by_id(user_id, db)
    data = None
    if del_user_id is not None:
        data = DeleteUserResponse(id=del_user_id)
    return DataDeleteUserResponse(
        data=data
    )
