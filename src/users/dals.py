from uuid import UUID

from sqlalchemy import and_, select, update
from sqlalchemy.ext.asyncio import AsyncSession

from src.core.models import User


class UserDAL:

    """Data Access Layer for operating user info
    (Слой для работы с ORM | БД).
    """

    def __init__(self, db_session: AsyncSession):
        """Initializing UserDAL."""
        self.db_session = db_session

    async def create_user(
        self,
        username: str,
        password: str,
        email: str,
        firstname: str | None = None,
        lastname: str | None = None,
        status: str | None = None,
    ) -> User:

        new_user = User(
            username=username,
            password=password,
            email=email,
            firstname=firstname,
            lastname=lastname,
            status=status,
        )
        self.db_session.add(new_user)
        await self.db_session.flush()
        return new_user

    async def get_user_by_id(self, user_id: UUID) -> User | None:
        query = select(User).where(and_(User.id == user_id, User.is_active == True))
        res = await self.db_session.execute(query)
        user_row = res.fetchone()
        if user_row is not None:
            return user_row[0]

    async def get_user_by_email(self, email: str) -> User | None:
        query = select(User).where(and_(User.email == email, User.is_active == True))
        res = await self.db_session.execute(query)
        user_row = res.fetchone()
        if user_row is not None:
            return user_row[0]

    async def update_user(self, user_id: UUID, **kwargs: dict) -> User | None:
            query = (
                update(User)
                .where(and_(User.id == user_id, User.is_active == True))
                .values(kwargs)
                .returning(User)
            )
            res = await self.db_session.execute(query)
            update_user_id_row = res.fetchone()
            if update_user_id_row is not None:
                return update_user_id_row[0]

    async def delete_user(self, user_id: UUID) -> UUID | None:
        query = (
            update(User)
            .where(and_(User.id == user_id, User.is_active == True))
            .values(is_active=False)
            .returning(User.id)
        )
        res = await self.db_session.execute(query)
        deleted_user_id_row = res.fetchone()
        if deleted_user_id_row is not None:
            return deleted_user_id_row[0]

