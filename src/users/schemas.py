from datetime import datetime
from uuid import UUID

from fastapi import HTTPException
from pydantic import BaseModel, EmailStr, field_validator

from src.common.regex import LETTER_MATCH_PATTERN
from src.common.schemas import TunedModel


class CreateUserRequest(BaseModel):

    """Create user with Pydantic."""

    username: str
    password: str
    email: EmailStr
    firstname: str | None = None
    lastname: str | None = None
    status: str | None = None

    @field_validator("firstname", "lastname")
    def validate_letters(cls, value: str) -> str | None:
        if value is None:
            return value

        if not LETTER_MATCH_PATTERN.match(value):
            raise HTTPException(
                status_code=400, detail="Field must contain only letters"
            )

        return value


class UpdateUserRequest(BaseModel):

    """Update user with Pydantic."""

    username: str | None = None
    password: str | None = None
    email: EmailStr | None = None
    firstname: str | None = None
    lastname: str | None = None
    status: str | None = None

    @field_validator("firstname", "lastname")
    def validate_letters(cls, value: str) -> str | None:
        if value is None:
            return value

        if not LETTER_MATCH_PATTERN.match(value):
            raise HTTPException(
                status_code=400, detail="Field must contain only letters"
            )

        return value


class ProfileUserResponse(TunedModel):

    """Response after retrieve/create/update user with Pydantic."""

    id: UUID
    username: str
    email: EmailStr
    firstname: str | None
    lastname: str | None
    status: str | None
    is_active: bool
    created_at: datetime


class Token(BaseModel):

    """Response after auth user, get token"""
    access_token: str
    token_type: str


class DataProfileUserResponse(TunedModel):

    """Wrapper for response, data object and response."""

    data: ProfileUserResponse


class DeleteUserResponse(TunedModel):

    """Response after delete user with Pydantic."""

    id: UUID


class DataDeleteUserResponse(TunedModel):

    """Wrapper response after delete user with Pydantic."""

    data: DeleteUserResponse | None
