from sqlalchemy.ext.asyncio import AsyncSession

from src.common.hashing import Hasher
from src.core.models import User
from src.users.dals import UserDAL


async def _auth_user(email: str, password: str, session: AsyncSession) -> User | None:
    async with session.begin():
        user_dal = UserDAL(session)
        user = await user_dal.get_user_by_email(email)
        if user is None:
            return None

        if not Hasher.verify_password(password, user.password):
            return None

        return user
