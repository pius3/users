from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from src.core.models import User
from src.users.dals import UserDAL

"""
Файл, работает с сессией в БД и сериализация с помощью Pydantic
"""


async def _create_new_user(body: dict, session: AsyncSession) -> User:
    async with session.begin():
        user_dal = UserDAL(session)
        user = await user_dal.create_user(
            **body
        )
        return user


async def _get_user_by_id(user_id: UUID, session: AsyncSession) -> User | None:
    async with session.begin():
        user_dal = UserDAL(session)
        user = await user_dal.get_user_by_id(user_id)
        if user is not None:
            return user


async def _get_user_by_email(email: str, session: AsyncSession) -> User | None:
    async with session.begin():
        user_dal = UserDAL(session)
        user = await user_dal.get_user_by_email(email)
        if user is not None:
            return user


async def _update_user_by_id(user_id: UUID, updated_user_data: dict, session: AsyncSession) -> User | None:
    async with session.begin():
        user_dal = UserDAL(session)
        upd_user = await user_dal.update_user(user_id, **updated_user_data)
        return upd_user


async def _delete_user_by_id(user_id: UUID, session: AsyncSession) -> UUID | None:
    async with session.begin():
        user_dal = UserDAL(session)
        del_user = await user_dal.delete_user(user_id)
        return del_user
