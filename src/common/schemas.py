from pydantic import BaseModel


class TunedModel(BaseModel):

    """Tells about pydantic to convert even non dict obj to json."""

    class Config:
        orm_mode = True


class ErrorResponse(BaseModel):

    """Custom response model for errors."""

    message: str
    code: int


class ErrorsResponse(BaseModel):

    """Wrapper custom response model for errors."""

    errors: ErrorResponse
