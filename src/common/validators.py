import re


def validate_email(email: str) -> str:
    """Validate email."""
    if not isinstance(email, str):
        raise ValueError("Email is not a string")

    if not re.match(r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$", email):
        raise ValueError("Email некорректен")

    return email
