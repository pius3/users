from factory import alchemy


class AlchemyModelFactory(alchemy.SQLAlchemyModelFactory):

    """Factory for SQLAlchemyModel"""
